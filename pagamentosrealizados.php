<?php

session_start();

if (isset($_SESSION['usuarioNome'])) {

}else{
  header('location: index.php');
}

require_once 'crud/crud_despesa.php';
require_once 'bancodedados/conexao.php';

$resultado_despesa =  "SELECT * FROM despesa WHERE Status = 'Realizado'";
$resultados =  mysqli_query($conn, $resultado_despesa);


 ?>


 <!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <link rel="stylesheet" href="css/escolhas.css">
     <link rel="stylesheet" href="css/clientes.css">
     <link rel="stylesheet" type="text/css" href="css/universal.css">
     <link rel="stylesheet" type="text/css" href="css/agenda.css">
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
     <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
     <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
     <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


    <title>Pagamentos Realizados</title>
  </head>
  <body>
   <nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #cd84f1; font-family: Century Gothic;">
    <a class="navbar-brand" href="escolha.php" style="color: #FFFAFA; font-size: 17px;">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse justify-content-center" id="navbarNav">

      <ul class="navbar-nav">
        <li class="nav-item" style="margin-right: 30px;">
          <a class="nav-link" href="escolha.php" style="color: #FFFAFA; font-size: 17px;">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item" style="margin-right: 30px;">
          <a class="nav-link" href="#" style="color: #FFFAFA; font-size: 17px;">Fidelidade</a>
        </li>
       <li class="nav-item dropdown" style="margin-right: 30px;">
        <a class="nav-link dropdown-toggle" href="escolha.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #FFFAFA; font-size: 17px;">
          Financeiro
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="despesa.php" style="font-size: 15px;">Despesas</a>
          <a class="dropdown-item" href="comissao.php" style="font-size: 15px;">Comissão</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="pagamentosrealizados.php" style="font-size: 15px;">Pagamentos Realizados</a>
          <a class="dropdown-item" href="despesa.php" style="font-size: 15px;">Pagamentos não realizados</a>
        </div>
      </li>
          <li class="nav-item dropdown" style="margin-right: 30px;">
        <a class="nav-link dropdown-toggle" href="escolha.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #FFFAFA; font-size: 17px;">
          Cadastro
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="clientes.php" style="font-size: 15px;">Cliente</a>
          <a class="dropdown-item" href="agenda.php" style="font-size: 15px;">Agenda</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="funcionarios.php" style="font-size: 15px;">Funcionário</a>
        </div>
      </li>
        <li class="nav-item dropdown" style="margin-right: 30px;">
        <a class="nav-link dropdown-toggle" href="escolha.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #FFFAFA; font-size: 17px;">
         Visualizar
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="listaagenda.php" style="font-size: 15px;">Agenda</a>
          <a class="dropdown-item" href="listaclientes.php" style="font-size: 15px;">Clientes</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="listafuncionarios.php" style="font-size: 15px;">Funcionários</a>
          <a class="dropdown-item" href="comissao.php" style="font-size: 15px;">Comissões</a>
        </div>
      </li>
        <li class="nav-item dropdown" style="margin-right: 30px;">
        <a class="nav-link dropdown-toggle" href="escolha.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #FFFAFA; font-size: 17px;">
          <i class="fas fa-user-circle"></i> <?=$_SESSION['usuarioNome']?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php" style="font-size: 15px;">Sair</a>
          <a class="dropdown-item" href="agenda.php" style="font-size: 15px;">Redefinir Senha</a>

        </div>
      </li>

      </ul>
    </div>
  </nav>
<br>
  <div class="p" id="p" style="text-align: center; font-size: 20px;font-family: 'Ubuntu Mono', monospace; color: #FF1493;">
     <p id="p"> <strong>Pagamentos Realizados</strong></p>
   </div>

   <hr>



   <div class="container">

<a href="gerarpdf.php" data-toggle="tooltip" title="Relátorio"><button type="button" class="btn btn-warning" style="color: white;"><i class="fas fa-file"></i></button></a>
<a href="escolha.php" data-toggle="tooltip" title="Voltar para tela inicial"><button type="button" class="btn btn-danger"><i class="fas fa-sign-out-alt"></i></button></a>



<hr>

<br>
    <div class="agenda">
        <div class="table-responsive">
            <table class="table table-condensed table-bordered">
                <thead>
                    <tr>
                        <th style="font-size: 15px; text-align: center; font-family: Century Gothic;">Descrição</th>
                        <th style="font-size: 15px; text-align: center; font-family: Century Gothic;">Dia Pago</th>
                        <th style="font-size: 15px; text-align: center; font-family: Century Gothic;">Valor</th>
                        <th style="font-size: 15px; text-align: center; font-family: Century Gothic;">Observação</th>
                    </tr>
                </thead>
                <tbody >
                    <?php while ($row = mysqli_fetch_assoc($resultados)) { ?>
                    <!-- Single event in a single day -->
                    <tr>
                        <td class="agenda-date" class="active" rowspan="1" style="font-size: 15px; text-align: center; font-family: 'Ubuntu Mono', monospace;">
                            <?php echo $row['Descricao'];?></div>

                        </td>
                        <td class="agenda-time" style="font-size: 15px; text-align: center; font-family: 'Ubuntu Mono', monospace;">
                            <?php echo date("d/m/Y", strtotime($row['DataPago'])); ?>
                        </td>
                        <td class="agenda-events" style="font-size: 15px; text-align: center; font-family: 'Ubuntu Mono', monospace;">
                            <div class="agenda-event">
                                 <?php echo $row['Valor'];?>
                            </div>
                        </td>
                         <td class="agenda-events" style="font-size: 15px; text-align: center; font-family: 'Ubuntu Mono', monospace;">
                            <div class="agenda-event">
                                 <?php echo $row['ObsPagamento'];?>
                            </div>
                        </td>
                    </tr>
                    <?php } ?>
                    <!-- Multiple events in a single day (note the rowspan) -->

                </tbody>
            </table>

        </div>
    </div>
</div>

</div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>