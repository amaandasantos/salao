<?php

session_start();

if (isset($_SESSION['usuarioNome'])) {

}else{
  header('location: index.php');
}

//BUSCANDO AS CLASSES
require_once 'crud/crud_despesa.php';
require_once 'bancodedados/conexao.php';

$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
$resultado_despesa = "SELECT * FROM despesa WHERE Id = '$id'";
$resultados_despesas = mysqli_query($conn, $resultado_despesa);
$row_d = mysqli_fetch_assoc($resultados_despesas);

//ESTANCIANDO A CLASSES
$objFunc = new despesa();

if(isset($_POST['btn_alterar'])){
  $objFunc->Update($_POST);
  header('location: despesa.php');
}




?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <link rel="stylesheet" href="css/escolhas.css">
     <link rel="stylesheet" href="css/clientes.css">
     <link rel="stylesheet" type="text/css" href="css/universal.css">
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Clientes</title>
  </head>
  <body>
 <nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #cd84f1; font-family: Century Gothic;">
    <a class="navbar-brand" href="escolha.php">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse justify-content-center" id="navbarNav">

      <ul class="navbar-nav">
        <li class="nav-item" style="margin-right: 30px;">
          <a class="nav-link" href="escolha.php" style="color: #FFFAFA">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item" style="margin-right: 30px;">
          <a class="nav-link" href="#" style=" color: #FFFAFA; font-family: Century Gothic;">Fidelidade</a>
        </li>
       <li class="nav-item dropdown" style="margin-right: 30px;">
        <a class="nav-link dropdown-toggle" href="escolha.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style=" color: #FFFAFA; font-family: Century Gothic;">
          Financeiro
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="despesa.php">Despesas</a>
          <a class="dropdown-item" href="comissao.php">Comissão</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="pagamentosrealizados.php">Pagamentos Realizados</a>
          <a class="dropdown-item" href="despesa.php">Pagamentos não realizados</a>
        </div>
      </li>
          <li class="nav-item dropdown" style="margin-right: 30px;">
        <a class="nav-link dropdown-toggle" href="escolha.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style=" color: #FFFAFA; font-family: Century Gothic;">
          Cadastro
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="clientes.php">Cliente</a>
          <a class="dropdown-item" href="agenda.php">Agenda</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="funcionarios.php">Funcionário</a>
        </div>
      </li>
        <li class="nav-item dropdown" style="margin-right: 30px;">
        <a class="nav-link dropdown-toggle" href="escolha.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style=" color: #FFFAFA; font-family: Century Gothic;">
         Visualizar
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="listaagenda.php">Agenda</a>
          <a class="dropdown-item" href="listaclientes.php">Clientes</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="listafuncionarios.php">Funcionários</a>
          <a class="dropdown-item" href="comissao.php">Comissões</a>
        </div>
      </li>
        <li class="nav-item dropdown" style="margin-right: 30px;">
        <a class="nav-link dropdown-toggle" href="escolha.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style=" color: #FFFAFA; font-family: Century Gothic;">
          <i class="fas fa-user-circle"></i> <?=$_SESSION['usuarioNome']?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php">Sair</a>
          <a class="dropdown-item" href="agenda.php">Redefinir Senha</a>

        </div>
      </li>

      </ul>
    </div>
  </nav>
<br>
  <div class="p" id="p" style="text-align: center; font-size: 20px;">
     <p id="p"> <strong>Atualizando dados da despesa: <?php echo $row_d['Descricao']; ?></strong> </p>
   </div>

   <hr>

    <div class="col pt-lg-1 pb-2" id="divexterna">
    <div class="col-lg-6 col-sm-12 mt-5 col-md-6 p-0 " id="divinterna" >
   <form method="post">
<input type="hidden" name="id" value="<?php echo $row_d['Id']; ?>">
  <div class="form-group">
   <div class="form-row mt-3 ">
          <div class="form-group col-md-3">
           <label for="exampleFormControlInput1">Descrição</label>
           <input type="text" class="form-control" id="exampleFormControlInput1" name="descricao" placeholder="Sobre a despesa"value="<?php echo $row_d['Descricao']; ?>">
          </div>
          <div class="form-group col-md-3">
           <label for="exampleFormControlInput1">Data da despesa:</label>
           <input type="text" class="form-control" name="data" id="exampleFormControlInput1" value="<?php echo $row_d['Data']; ?>">
          </div>

          <div class="form-group col-md-3">
           <label for="inputCPF">Forma de Pagamento:</label>
            <input type="text" class="form-control" id="pagamento" placeholder="Forma de Pagamento" name="pagamento" value="<?php echo $row_d['Pagamento']; ?>">
          </div>

          <div class="form-group col-md-3">
           <label for="exampleFormControlInput1">Valor:</label>
           <input type="text" class="form-control" name="valor" id="exampleFormControlInput1" placeholder="EX.: 150,00" value="<?php echo $row_d['Valor']; ?>">
          </div>
</div>

 <div class="form-row mt-3 ">
          <div class="form-group col-md-12">
            <label for="exampleFormControlTextarea1">Observação:</label>
            <textarea class="form-control" name="observacao"  id="exampleFormControlTextarea1" rows="6"><?php echo $row_d['Observacao']; ?></textarea>
          </div>
</div>


<br>
<div class="col-lg-12" align="center" >

          <button type="submit" class="btn btn-light" name="btn_alterar" id="btn_alterar" style="background-color: #DC8CF2; color: white;">Alterar</button>
          <a href="escolha.php"><button type="button" class="btn btn-light" style="background-color: #F28CCB; color: white;">Cancelar</button></a>
   </div>

</div>
</div>
</div>
</form>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>