-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 26-Mar-2019 às 20:20
-- Versão do servidor: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `salon`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `agenda`
--

DROP TABLE IF EXISTS `agenda`;
CREATE TABLE IF NOT EXISTS `agenda` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Cliente` varchar(500) DEFAULT NULL,
  `Horario` varchar(500) DEFAULT NULL,
  `Data` varchar(500) DEFAULT NULL,
  `Procedimento` varchar(500) DEFAULT NULL,
  `Formapg` varchar(500) DEFAULT NULL,
  `Receber` varchar(500) DEFAULT NULL,
  `Telefone` varchar(500) DEFAULT NULL,
  `Status` varchar(500) DEFAULT NULL,
  `Pagamento` varchar(500) DEFAULT NULL,
  `DataRecebimento` varchar(500) DEFAULT NULL,
  `Observacao` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `agenda`
--

INSERT INTO `agenda` (`Id`, `Cliente`, `Horario`, `Data`, `Procedimento`, `Formapg`, `Receber`, `Telefone`, `Status`, `Pagamento`, `DataRecebimento`, `Observacao`) VALUES
(3, 'Filipe', '12:00', '2019-02-26', 'uu', 'uu', '500,00', '(81) 9999-9999', 'Finalizado', 'Recebido', '2019-03-08', 'ddd'),
(4, 'Filipe', '15:00', '2019-02-26', 'Progressiva', 'uu', '200,00', '(81) 9999-9999', 'Finalizado', NULL, '2019-03-08', 'oo'),
(5, 'Filipe', '19:00', '27/03/2019', 'Pintura', 'Dinheiro', '900,00', '(81) 9999-9999', 'Ativo', NULL, NULL, NULL),
(6, 'Amanda', '16:00', '2019-03-09', 'Corte', 'CartÃ£o de CrÃ©dito', '500,00', '81999443759', 'Finalizado', NULL, '2019-03-08', 'fez mais de um procedimento'),
(7, 'Cida', '24:00', '31/03/2017', 'Pintura', 'CartÃ£o de DÃ©bito', '450,00', '81999443759', 'Ativo', NULL, NULL, NULL),
(8, 'Cida', '18:00', '2019-03-26', 'Corte', 'CartÃ£o de CrÃ©dito', '500.00', '(81)99944-3759', 'Ativo', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

DROP TABLE IF EXISTS `cliente`;
CREATE TABLE IF NOT EXISTS `cliente` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(500) DEFAULT NULL,
  `CPF` varchar(500) DEFAULT NULL,
  `RG` varchar(500) DEFAULT NULL,
  `Rua` varchar(500) DEFAULT NULL,
  `Bairro` varchar(500) DEFAULT NULL,
  `CEP` varchar(500) DEFAULT NULL,
  `Numero` varchar(500) DEFAULT NULL,
  `Telefone` varchar(500) DEFAULT NULL,
  `Status` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`Id`, `Nome`, `CPF`, `RG`, `Rua`, `Bairro`, `CEP`, `Numero`, `Telefone`, `Status`) VALUES
(1, 'Amanda', '3333333333', '88888', 'euclides', 'ddd', '55030410', '33', '(81) 9999-9999', NULL),
(2, 'Filipe', '3333333333', '88888', 'euclides', 'ddd', '55030410', '33', '(81) 9999-9999', 'Ativo'),
(3, 'Cida', '898989898989', '8989898989', 'barao', 'pe', '9898989898', '123', '81999443759', 'Ativo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `comissao`
--

DROP TABLE IF EXISTS `comissao`;
CREATE TABLE IF NOT EXISTS `comissao` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Funcionario` varchar(500) DEFAULT NULL,
  `Comissao` varchar(500) DEFAULT NULL,
  `Data` varchar(500) DEFAULT NULL,
  `Situacao` varchar(500) DEFAULT NULL,
  `Pagante` varchar(500) DEFAULT NULL,
  `Forma` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `comissao`
--

INSERT INTO `comissao` (`Id`, `Funcionario`, `Comissao`, `Data`, `Situacao`, `Pagante`, `Forma`) VALUES
(1, 'Filipe', 'aa', '2019-03-15', NULL, 'aa', 'aa'),
(2, 'Filipe', '200,00', '2019-03-16', NULL, 'Amanda Feminino', 'Cheque'),
(3, 'Filipe', '200,00', '2019-03-15', 'Outro', 'Amanda Feminino', 'Cheque');

-- --------------------------------------------------------

--
-- Estrutura da tabela `despesa`
--

DROP TABLE IF EXISTS `despesa`;
CREATE TABLE IF NOT EXISTS `despesa` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Descricao` varchar(500) DEFAULT NULL,
  `Data` varchar(500) DEFAULT NULL,
  `Pagamento` varchar(500) DEFAULT NULL,
  `Valor` float DEFAULT NULL,
  `Observacao` varchar(500) DEFAULT NULL,
  `Status` varchar(500) DEFAULT NULL,
  `ObsPagamento` varchar(500) DEFAULT NULL,
  `DataPago` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `despesa`
--

INSERT INTO `despesa` (`Id`, `Descricao`, `Data`, `Pagamento`, `Valor`, `Observacao`, `Status`, `ObsPagamento`, `DataPago`) VALUES
(1, 'Meia', '23/08/2013', 'Dinheiro', 508.58, 'comprada na zara', 'Realizado', 'pago hoje', '2019-03-20'),
(2, NULL, NULL, NULL, 303, NULL, 'Inativo', NULL, NULL),
(3, 'Pagamento atrasado', '2019-03-15', 'CartÃ£o de CrÃ©dito', 200.89, 'ss', 'Ativo', NULL, NULL),
(4, 'Novo shampoo', '2019-03-16', 'Dinheiro', 200, 'dd', 'Ativo', NULL, NULL),
(5, 'Secador', '2019-03-21', 'CartÃ£o de CrÃ©dito', 200.89, 'Comprado em 2x', 'Ativo', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `financeiro`
--

DROP TABLE IF EXISTS `financeiro`;
CREATE TABLE IF NOT EXISTS `financeiro` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Valor` varchar(500) NOT NULL,
  `Cliente` varchar(500) NOT NULL,
  `Data` varchar(500) NOT NULL,
  `Observacao` varchar(500) NOT NULL,
  `Recebido` varchar(500) NOT NULL,
  `Id_agenda` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `Id_agenda` (`Id_agenda`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionario`
--

DROP TABLE IF EXISTS `funcionario`;
CREATE TABLE IF NOT EXISTS `funcionario` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(500) NOT NULL,
  `CPF` varchar(500) NOT NULL,
  `RG` varchar(500) NOT NULL,
  `DataNascimento` varchar(500) NOT NULL,
  `Idade` int(11) NOT NULL,
  `Sexo` varchar(500) NOT NULL,
  `Telefone` varchar(500) NOT NULL,
  `Email` varchar(500) NOT NULL,
  `Rua` varchar(500) NOT NULL,
  `Bairro` varchar(500) NOT NULL,
  `CEP` varchar(500) NOT NULL,
  `Cidade` varchar(500) NOT NULL,
  `UF` varchar(500) NOT NULL,
  `Nacionalidade` varchar(500) NOT NULL,
  `Numero` int(11) NOT NULL,
  `Complemento` varchar(500) NOT NULL,
  `Status` varchar(500) NOT NULL,
  `Salario` varchar(500) NOT NULL,
  `PagoPor` varchar(500) NOT NULL,
  `DataPagamento` varchar(500) NOT NULL,
  `Valor` float NOT NULL,
  `Observacao` varchar(500) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `funcionario`
--

INSERT INTO `funcionario` (`Id`, `Nome`, `CPF`, `RG`, `DataNascimento`, `Idade`, `Sexo`, `Telefone`, `Email`, `Rua`, `Bairro`, `CEP`, `Cidade`, `UF`, `Nacionalidade`, `Numero`, `Complemento`, `Status`, `Salario`, `PagoPor`, `DataPagamento`, `Valor`, `Observacao`) VALUES
(1, 'Filipe', '70288902440', '9517155', '27/03/1997', 21, 'Feminino', '81999443759', 'amanda@gmail.com', 'BarÃ£o de Pirangi', 'PetrÃ³polis', '55030410', 'Caruaru', 'PE', 'Brasieleira', 124, 'casa', 'Ativo', 'Pago', 'Amanda', '2019-03-14', 500, 'atrasado');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(500) NOT NULL,
  `Senha` varchar(500) NOT NULL,
  `Email` varchar(500) NOT NULL,
  `Status` varchar(500) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`Id`, `Nome`, `Senha`, `Email`, `Status`) VALUES
(1, 'Amanda', '123456', 'amanda@gmail.com', 'Ativo');

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `financeiro`
--
ALTER TABLE `financeiro`
  ADD CONSTRAINT `financeiro_ibfk_1` FOREIGN KEY (`Id_agenda`) REFERENCES `agenda` (`Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
