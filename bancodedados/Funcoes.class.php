<?php
//CRIANDO A CLASSE
class Funcoes{
    //METODO RESPONSAVEL POR TRATAR OS CARACTERES
    public function tratarCaracter($vlr, $tipo){
        switch($tipo){
            case 1: $rst = utf8_decode($vlr); break;
            case 2: $rst = htmlentities($vlr, ENT_QUOTES, "ISO-8859-1"); break;
        }
        return $rst;
    }


    //METODO PARA ACEITAR SO NUMERO
    function soNumero($str) {
        return preg_replace("/[^0-9]/", "", $str);
    }



    //RECUPERA A DATA E HORA ATUAL
    public function dataAtual($vlr){
        switch($vlr){
            case 1: return date("Y-m-d"); break;
            case 2: return date("Y-m-d H:i:s"); break;
            case 3: return date("d/m/Y"); break;
        }
    }

    //RESPONSAVEL POR COLOCAR QUALQUER VALOR EM base64
    public function base64($vlt, $n){
        switch($n){
            case 1: return base64_encode($vlt); break;
            case 2; return base64_decode($vlt); break;
        }
    }

    //VERIFICAR CAMPO VAZIO
    public function verificarCampo($dado){
        return (isset($dado))?($dado):("");
    }

    public function normalizaString($str){
        $de = array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/","/(ç)/","/(Ç)/", "/(\(|\)|,| |\/)/");
        $para = explode(" ","a A e E i I o O u U n N c C -");  $i = 1;
        $str = preg_replace($de, $para, strtolower($str));
        while($i > 0){
            $str = str_replace('--','-',$str,$i);
            if(substr($str, -1) == '-'){
                $str = substr($str, 0, -1);
            }
        }
        return $str;
    }


}

?>