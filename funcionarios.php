<?php

session_start();

if (isset($_SESSION['usuarioNome'])) {

}else{
  header('location: index.php');
}

//BUSCANDO AS CLASSES
require_once 'crud/crud_funcionarios.php';
require_once 'bancodedados/conexao.php';
//ESTANCIANDO A CLASSES
$objFunc = new funcionarios();

if(isset($_POST['btn_cadastrar'])){
  $objFunc->Insert($_POST);
  header('location: listafuncionarios.php');
}



?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <link rel="stylesheet" href="css/escolhas.css">
     <link rel="stylesheet" href="css/clientes.css">
     <link rel="stylesheet" type="text/css" href="css/universal.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

       <script type="text/javascript">
      function ValidaCPF(){
  var RegraValida=document.getElementById("RegraValida").value;
  var cpfValido = /^(([0-9]{3}.[0-9]{3}.[0-9]{3}-[0-9]{2})|([0-9]{11}))$/;
  if (cpfValido.test(RegraValida) == true)  {
  console.log("CPF Válido");
  } else  {
  console.log("CPF Inválido");
  }
    }
  function fMasc(objeto,mascara) {
obj=objeto
masc=mascara
setTimeout("fMascEx()",1)
}

  function fMascEx() {
obj.value=masc(obj.value)
}

   function mCPF(cpf){
cpf=cpf.replace(/\D/g,"")
cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2")
cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2")
cpf=cpf.replace(/(\d{3})(\d{1,2})$/,"$1-$2")
return cpf
}

   function mRG(rg){
rg=rg.replace(/\D/g,"")
rg=rg.replace(/(\d{1})(\d)/,"$1.$2")
rg=rg.replace(/(\d{3})(\d)/,"$1.$2")
rg=rg.replace(/(\d{3})(\d{1,2})$/,"$1-$2")
return rg
}

   function mCEP(cep){
cep=cep.replace(/\D/g,"")
cep=cep.replace(/(\d{2})(\d)/,"$1.$2")
cep=cep.replace(/(\d{3})(\d)/,"$1-$2")
cep=cep.replace(/(\d{3})(\d{1,2})$/,"$1-$2")
return cep
}

   function mTEL(tel){
tel=tel.replace(/\D/g,"")
tel=tel.replace(/(\d{1})(\d)/,"($1$2)")
tel=tel.replace(/(\d{5})(\d)/,"$1-$2")
tel=tel.replace(/(\d{4})(\d{1,2})$/,"$1-$2")
return tel
}


    </script>

    <title>Funcionários</title>


  </head>
  <body>
  <nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #cd84f1; font-family: Century Gothic;">
    <a class="navbar-brand" href="escolha.php">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse justify-content-center" id="navbarNav">

      <ul class="navbar-nav">
        <li class="nav-item" style="margin-right: 30px;">
          <a class="nav-link" href="escolha.php" style="color: #FFFAFA">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item" style="margin-right: 30px;">
          <a class="nav-link" href="#" style=" color: #FFFAFA; font-family: Century Gothic;">Fidelidade</a>
        </li>
       <li class="nav-item dropdown" style="margin-right: 30px;">
        <a class="nav-link dropdown-toggle" href="escolha.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style=" color: #FFFAFA; font-family: Century Gothic;">
          Financeiro
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="despesa.php">Despesas</a>
          <a class="dropdown-item" href="comissao.php">Comissão</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="pagamentosrealizados.php">Pagamentos Realizados</a>
          <a class="dropdown-item" href="despesa.php">Pagamentos não realizados</a>
        </div>
      </li>
          <li class="nav-item dropdown" style="margin-right: 30px;">
        <a class="nav-link dropdown-toggle" href="escolha.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style=" color: #FFFAFA; font-family: Century Gothic;">
          Cadastro
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="clientes.php">Cliente</a>
          <a class="dropdown-item" href="agenda.php">Agenda</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="funcionarios.php">Funcionário</a>
        </div>
      </li>
        <li class="nav-item dropdown" style="margin-right: 30px;">
        <a class="nav-link dropdown-toggle" href="escolha.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style=" color: #FFFAFA; font-family: Century Gothic;">
         Visualizar
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="listaagenda.php">Agenda</a>
          <a class="dropdown-item" href="listaclientes.php">Clientes</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="listafuncionarios.php">Funcionários</a>
          <a class="dropdown-item" href="comissao.php">Comissões</a>
        </div>
      </li>
        <li class="nav-item dropdown" style="margin-right: 30px;">
        <a class="nav-link dropdown-toggle" href="escolha.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style=" color: #FFFAFA; font-family: Century Gothic;">
          <i class="fas fa-user-circle"></i> <?=$_SESSION['usuarioNome']?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php">Sair</a>
          <a class="dropdown-item" href="agenda.php">Redefinir Senha</a>

        </div>
      </li>

      </ul>
    </div>
  </nav>
<br>
  <div class="p" id="p" style="text-align: center; font-size: 20px; font-family: 'Ubuntu Mono', monospace; color: #FF1493;">
     <p id="p"> <strong>Controle de Funcionários</strong> </p>
   </div>
   <hr>
    <div class="col pt-lg-1 pb-2" id="divexterna">
    <div class="col-lg-6 col-sm-12 mt-5 col-md-6 p-0 " id="divinterna" >
   <form method="post" style="font-family: 'Marmelad', sans-serif;">

  <div class="form-group">
   <div class="form-row mt-3 ">
          <div class="form-group col-md-4">
            <label for="inputNomeCompleto">Nome</label>
            <input type="text" class="form-control" id="nome" placeholder="Nome" required name="nome">
          </div>
          <div class="form-group col-md-4">
           <label for="inputCPF">CPF:</label>
            <input type="text" class="form-control" id="cpf" placeholder="CPF" name="cpf" onkeydown="javascript: fMasc( this, mCPF );" maxlength="14">
          </div>

          <div class="form-group col-md-4">
           <label for="inputCPF">RG:</label>
            <input type="text" class="form-control" id="rg" placeholder="RG" name="rg" onkeydown="javascript: fMasc( this, mRG );" maxlength="9">
          </div>

</div>


   <div class="form-row mt-3 ">
          <div class="form-group col-md-4">
            <label for="inputNomeCompleto">Data Nascimento:</label>
            <input type="text" class="form-control" id="dt" placeholder="Ex.: 27/03/1997" required name="dt">
          </div>
          <div class="form-group col-md-4">
           <label for="inputCPF">Idade:</label>
            <input type="number" class="form-control" id="idade" placeholder="Idade" name="idade">
          </div>

         <div class="form-group col-md-4">
            <label for="inputNomeCompleto">Sexo:</label>
            <select class="form-control" name="sexo">
              <option>Feminino</option>
              <option>Masculino</option>
              <option>Outro</option>
            </select>
          </div>
</div>

 <div class="form-row mt-3 ">
          <div class="form-group col-md-4">
            <label for="inputNomeCompleto">Telefone:</label>
            <input type="text" class="form-control" id="telefone" placeholder="(81) 99944-3759" name="telefone" onkeydown="javascript: fMasc( this, mTEL );" maxlength="14">
          </div>
          <div class="form-group col-md-8">
           <label for="inputCPF">E-mail:</label>
            <input type="email" class="form-control" id="email" placeholder="E-mail" name="email">
          </div>


</div>

 <div class="form-row mt-3 ">
          <div class="form-group col-md-4">
            <label for="inputNomeCompleto">Rua:</label>
            <input type="text" class="form-control" id="rua" placeholder="Rua" name="rua">
          </div>
          <div class="form-group col-md-4">
           <label for="inputCPF">Bairro:</label>
            <input type="text" class="form-control" id="bairro" placeholder="Bairro" name="bairro">
          </div>

          <div class="form-group col-md-4">
           <label for="inputCPF">CEP:</label>
            <input type="text" class="form-control" id="cep" placeholder="CEP" name="cep" onkeydown="javascript: fMasc( this, mCEP );" maxlength="10">
          </div>
</div>

 <div class="form-row mt-3 ">
          <div class="form-group col-md-4">
            <label for="inputNomeCompleto">Cidade:</label>
            <input type="text" class="form-control" id="cidade" placeholder="Cidade" name="cidade">
          </div>
          <div class="form-group col-md-4">
           <label for="inputCPF">UF:</label>
            <input type="text" class="form-control" id="uf" placeholder="UF" name="uf">
          </div>

          <div class="form-group col-md-4">
           <label for="inputCPF">Nacionalidade:</label>
            <input type="text" class="form-control" id="nacionalidade" placeholder="Nacionalidade" name="nacionalidade">
          </div>
</div>



<div class="form-row mt-3 ">
          <div class="form-group col-md-4">
            <label for="inputNomeCompleto">Número:</label>
            <input type="number" class="form-control" id="numero" placeholder="Número" name="numero">
          </div>
          <div class="form-group col-md-4">
           <label for="inputCPF">Complemento:</label>
            <input type="text" class="form-control" id="complemento" placeholder="Complemento" name="complemento">
          </div>

          <div class="form-group col-md-4" style="display: none;">
           <label for="inputCPF">Status:</label>
            <input type="text" class="form-control" id="status" placeholder="Ativo" name="status" value="ATIVO" disabled>
          </div>
</div>
<br>
<div class="col-lg-12" align="center" >

          <button type="submit" class="btn btn-light" name="btn_cadastrar" id="btn_cadastrar" style="background-color: #DC8CF2; color: white;">Cadastrar</button>
          <a href="escolha.php"><button type="button" class="btn btn-light" style="background-color: #F28CCB; color: white;">Cancelar</button></a>
   </div>

</div>
</div>
</div>
</form>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>