<?php

session_start();
if (isset($_SESSION['usuarioNome'])) {
  session_destroy();
}
//BUSCANDO AS CLASSES
require_once 'bancodedados/conexao.php';
require_once 'crud/crud_login.php';
//ESTANCIANDO A CLASSES
$objFunc = new Login();
//FAZENDO O LOGIN
if(isset($_POST['btn_login'])){
  $objFunc->logaUsuario($_POST);
}







?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
  <link href="//cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  <link rel="icon" href="https://cdn3.iconfinder.com/data/icons/happily-colored-snlogo/128/medium.png">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <!-- My CSS -->
  <link rel="stylesheet" href="index.css">

    <title>Hello, world!</title>

  </head>
  <body id="bd">


	<div class="had-container">

	     <div class="parallax-container logueo">
      	<div class="parallax"><img src="img/bgfundo.png"></div>
      	<div class="row"><br>
      		<div class="col m8 s8 offset-m2 offset-s2 center">
      			<h4 class="truncate bg-card-user">
      				<img src="img/logo.png" alt="" class="circle responsive-img">
					  <div class="row login">
					  	<h4>Login</h4>
					    <form method="post" class="col s12">
					      <div class="row">
					         <div class="input-field col m12 s12">
					          <i class="material-icons iconis prefix">account_box</i>
					          <input id="icon_prefix" type="text" class="validate" name="email">
					          <label for="icon_prefix">E-mail de acesso:</label>
					        </div>
					      </div>
					      <div class="row">
					        <div class="input-field col m12 s12">
					          <i class="material-icons iconis prefix">enhanced_encryption</i>
					          <input id="password" type="password" class="validate" name="senha">
					          <label for="password">Senha</label>
					        </div>
					      </div>
					      <div class="row">
                   <button type="submit" class="btn btn-danger" id="font" name="btn_login" id="btn_login">Entrar</button>
					      </div>
					    </form>
					  </div>
      			</h4>
		   	  </div>
	    	</div>
	    </div>
    </div>


    </div> <!-- fin del .container -->

<footer class="page-footer" id="footer">
    <div class="footer-copyright">
        <div class="container center">
        	Copyright © 2019 - To Develop
        </div>
    </div>
</footer>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <script src="index.js"></script>
  </body>
</html>