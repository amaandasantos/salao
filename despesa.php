<?php

session_start();

if (isset($_SESSION['usuarioNome'])) {

}else{
  header('location: index.php');
}

require_once 'crud/crud_despesa.php';
require_once 'bancodedados/conexao.php';

$resultado_despesa =  "SELECT * FROM despesa WHERE Status = 'Ativo'";
$resultados =  mysqli_query($conn, $resultado_despesa);


//ESTANCIANDO A CLASSES
$objFunc = new despesa();

if(isset($_POST['btn_despesa'])){
  $objFunc->Insert($_POST);
    header('location: despesa.php');
 }

 if(isset($_POST['btn_modal'])){
  $objFunc->UpdatePagamento($_POST);
    header('location: pagamentosrealizados.php');
 }



?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <link rel="stylesheet" href="css/escolhas.css">
     <link rel="stylesheet" href="css/clientes.css">
     <link rel="stylesheet" type="text/css" href="css/universal.css">
     <link rel="stylesheet" type="text/css" href="css/agenda.css">
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
     <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
     <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
     <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Clientes</title>
  </head>
  <body>
   <nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #cd84f1; font-family: Century Gothic;">
    <a class="navbar-brand" href="escolha.php" style="color: #FFFAFA; font-size: 17px;">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse justify-content-center" id="navbarNav">

      <ul class="navbar-nav">
        <li class="nav-item" style="margin-right: 30px;">
          <a class="nav-link" href="escolha.php" style="color: #FFFAFA; font-size: 17px;">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item" style="margin-right: 30px;">
          <a class="nav-link" href="#" style="color: #FFFAFA; font-size: 17px;">Fidelidade</a>
        </li>
       <li class="nav-item dropdown" style="margin-right: 30px;">
        <a class="nav-link dropdown-toggle" href="escolha.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #FFFAFA; font-size: 17px;">
          Financeiro
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="despesa.php" style="font-size: 15px;">Despesas</a>
          <a class="dropdown-item" href="comissao.php" style="font-size: 15px;">Comissão</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="pagamentosrealizados.php" style="font-size: 15px;">Pagamentos Realizados</a>
          <a class="dropdown-item" href="despesa.php" style="font-size: 15px;">Pagamentos não realizados</a>
        </div>
      </li>
          <li class="nav-item dropdown" style="margin-right: 30px;">
        <a class="nav-link dropdown-toggle" href="escolha.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #FFFAFA; font-size: 17px;">
          Cadastro
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="clientes.php" style="font-size: 15px;">Cliente</a>
          <a class="dropdown-item" href="agenda.php" style="font-size: 15px;">Agenda</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="funcionarios.php" style="font-size: 15px;">Funcionário</a>
        </div>
      </li>
        <li class="nav-item dropdown" style="margin-right: 30px;">
        <a class="nav-link dropdown-toggle" href="escolha.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #FFFAFA; font-size: 17px;">
         Visualizar
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="listaagenda.php" style="font-size: 15px;">Agenda</a>
          <a class="dropdown-item" href="listaclientes.php" style="font-size: 15px;">Clientes</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="listafuncionarios.php" style="font-size: 15px;">Funcionários</a>
          <a class="dropdown-item" href="comissao.php" style="font-size: 15px;">Comissões</a>
        </div>
      </li>
        <li class="nav-item dropdown" style="margin-right: 30px;">
        <a class="nav-link dropdown-toggle" href="escolha.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #FFFAFA; font-size: 17px;">
          <i class="fas fa-user-circle"></i> <?=$_SESSION['usuarioNome']?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php" style="font-size: 15px;">Sair</a>
          <a class="dropdown-item" href="agenda.php" style="font-size: 15px;">Redefinir Senha</a>

        </div>
      </li>

      </ul>
    </div>
  </nav>
<br>
  <div class="p" id="p" style="text-align: center; font-size: 20px; font-family: 'Ubuntu Mono', monospace; color: #FF1493;">
     <p id="p"> <strong>Suas Despesas</strong> </p>
   </div>

   <hr>



   <div class="container">
<a data-toggle="modal" data-target="#exampleModal" title="Nova despesa"><button type="button" class="btn btn-info"  alt="Finalizado" ><i class="fas fa-plus-circle"></i></button></a>
<a href="reldespesas.php" data-toggle="tooltip" title="Relátorio"><button type="button" class="btn btn-warning" style="color: white;"><i class="fas fa-file"></i></button></a>
<a href="escolha.php" data-toggle="tooltip" title="Voltar para tela inicial"><button type="button" class="btn btn-danger"><i class="fas fa-sign-out-alt"></i></button></a>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" style="font-family: Century Gothic; margin-right: 30px;">Cadastro de Despesas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

       <form method="post" style="font-family: Century Gothic;">
  <div class="form-group">
    <label for="exampleFormControlInput1">Descrição</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" name="descricao" placeholder="Sobre a despesa">
  </div>
   <div class="form-group">
    <label for="exampleFormControlInput1">Data da despesa:</label>
    <input type="date" class="form-control" name="data" id="exampleFormControlInput1">
  </div>
  <div class="form-group">
    <label for="exampleFormControlSelect1">Forma de Pagamento</label>
    <select class="form-control" id="exampleFormControlSelect1" name="pagamento">
      <option>Dinheiro</option>
      <option>Cartão de Crédito</option>
      <option>Cartão de Débito</option>
      <option>Boleto</option>
      <option>Cheque</option>
    </select>
  </div>
 <div class="form-group">
    <label for="exampleFormControlInput1">Valor:</label>
    <input type="text" class="form-control" name="valor" id="exampleFormControlInput1" placeholder="EX.: 150,00">
  </div>
  <div class="form-group">
    <label for="exampleFormControlTextarea1">Observação:</label>
    <textarea class="form-control" name="observacao" id="exampleFormControlTextarea1" rows="3"></textarea>
  </div>

  <div style="text-align: center;">
        <button type="submit" class="btn btn-danger" name="btn_despesa">Salvar</button>
        <button type="button" class="btn btn-info" data-dismiss="modal">Cancelar</button>
  </div>
</form>
      </div>

    </div>
  </div>
</div>

<hr>

<br>
    <div class="agenda">
        <div class="table-responsive">
            <table class="table table-condensed table-bordered">
                <thead>
                    <tr>
                        <th style="font-size: 15px; text-align: center; font-family: Century Gothic;">Dia Pagamento</th>
                        <th style="font-size: 15px; text-align: center; font-family: Century Gothic;">Valor</th>
                        <th style="font-size: 15px; text-align: center; font-family: Century Gothic;">Forma de Pagamento</th>
                        <th style="font-size: 15px; text-align: center; font-family: Century Gothic;">Descrição</th>
                    </tr>
                </thead>
                <tbody >
                    <?php while ($row = mysqli_fetch_assoc($resultados)) { ?>
                    <!-- Single event in a single day -->
                    <tr>
                        <td class="agenda-date" class="active" rowspan="1" style="font-size: 15px; text-align: center; font-family: 'Ubuntu Mono', monospace;">
                            <?php echo date("d/m/Y", strtotime($row['Data'])); ?></div>

                        </td>
                        <td class="agenda-time" style="font-size: 15px; text-align: center; font-family: 'Ubuntu Mono', monospace;">
                           <?php echo $row['Valor'];?>
                        </td>
                        <td class="agenda-events" style="font-size: 15px; text-align: center; font-family: 'Ubuntu Mono', monospace;">
                            <div class="agenda-event">
                                 <?php echo $row['Pagamento'];?>
                            </div>
                        </td>
                         <td class="agenda-events" style="font-size: 15px; text-align: center; font-family: 'Ubuntu Mono', monospace;">
                            <div class="agenda-event">
                                 <?php echo $row['Descricao'];?>
                            </div>
                        </td>



                        <td class="actions" style="font-size: 28px; color: white;">
                          <a data-toggle="modal" data-target="#modal" title="Status Atendimento" class="btn btn-success" onclick="setaDadosModal('<?php echo $row["Id"]; ?>')">
    <span class="btn-label"><i class="fa fa-check"></i></span>
</a>

<a href='editadespesas.php?id=" . <?php echo $row['Id'];?> . "'><button type="button" class="btn btn-danger" data-toggle="tooltip" title="Editar Despesa" style="background-color: #01DFD7;"><i class="fas fa-user-edit"></i></button></a>
                          <!--<button type="button" class="btn btn-warning" alt="Finalizado"><i class="fas fa-ban"></i></i></button>
                          <button type="button" class="btn btn-danger" alt="Finalizado"><i class="fas fa-trash-alt"></i></button> -->

          </td>
                    </tr>
                    <?php } ?>
                    <!-- Multiple events in a single day (note the rowspan) -->

                </tbody>
            </table>

        </div>
    </div>
</div>

<script>
function setaDadosModal(valor) {
    document.getElementById('id').value = valor;
}
</script>



<div id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Resumo do atendimento</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
            <div class="modal-body">
                <div class="panel-body">
                    <form id="modalExemplo" method="POST" action="">
                        <input type="text" name="id" id="id" style="display: none;">

                        <div class="form-group col-md-12">
           <label for="inputCPF">Status do pagamento</label>
            <select class="form-control" name="statusP">
              <option value=""></option>
              <option>Realizado</option>
              <option>Cancelado</option>
              <option>Dispensado</option>
              <option>Não Realizado</option>
            </select>
          </div>

            <div class="form-group col-md-12">
           <label for="inputValor">Data em que o pagamento foi realizado:</label>
            <input type="date" class="form-control" id="dataP" name="dataP" >
          </div>

          <div class="form-group col-md-12">
           <label for="inputValor">Observação:</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" name="obspagamento" rows="3"></textarea>
          </div>
<div class="col-lg-12" align="center" >

            <button type="submit" class="btn btn-info" id="btn_modal" name="btn_modal">Salvar</button>
         <button type="submit" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
   </div>
        </form>


      </div>

                </div>
            </div>
        </div>
    </div>
</div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>