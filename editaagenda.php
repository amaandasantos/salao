<?php

require_once 'bancodedados/conexao.php';
require_once 'crud/crud_agenda.php';

session_start();

if (isset($_SESSION['usuarioNome'])) {

}else{
  header('location: index.php');
}

$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
$resultado_agenda = "SELECT * FROM agenda WHERE Id = '$id'";
$result_agenda = mysqli_query($conn, $resultado_agenda);
$row_agenda = mysqli_fetch_assoc($result_agenda);

$objFunc = new agenda();

 if(isset($_POST['btn_alterar'])){
  $objFunc->updateAgenda($_POST);
   header('location: listaagenda.php');
}


?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <link rel="stylesheet" href="css/escolhas.css">
     <link rel="stylesheet" href="css/clientes.css">
     <link rel="stylesheet" type="text/css" href="css/universal.css">
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Clientes</title>
  </head>
  <body>

 <nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #cd84f1; font-family: Century Gothic;">
    <a class="navbar-brand" href="escolha.php">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse justify-content-center" id="navbarNav">

      <ul class="navbar-nav">
        <li class="nav-item" style="margin-right: 30px;">
          <a class="nav-link" href="escolha.php" style="color: #FFFAFA">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item" style="margin-right: 30px;">
          <a class="nav-link" href="#" style=" color: #FFFAFA; font-family: Century Gothic;">Fidelidade</a>
        </li>
       <li class="nav-item dropdown" style="margin-right: 30px;">
        <a class="nav-link dropdown-toggle" href="escolha.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style=" color: #FFFAFA; font-family: Century Gothic;">
          Financeiro
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="despesa.php">Despesas</a>
          <a class="dropdown-item" href="comissao.php">Comissão</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="pagamentosrealizados.php">Pagamentos Realizados</a>
          <a class="dropdown-item" href="despesa.php">Pagamentos não realizados</a>
        </div>
      </li>
          <li class="nav-item dropdown" style="margin-right: 30px;">
        <a class="nav-link dropdown-toggle" href="escolha.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style=" color: #FFFAFA; font-family: Century Gothic;">
          Cadastro
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="clientes.php">Cliente</a>
          <a class="dropdown-item" href="agenda.php">Agenda</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="funcionarios.php">Funcionário</a>
        </div>
      </li>
        <li class="nav-item dropdown" style="margin-right: 30px;">
        <a class="nav-link dropdown-toggle" href="escolha.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style=" color: #FFFAFA; font-family: Century Gothic;">
         Visualizar
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="listaagenda.php">Agenda</a>
          <a class="dropdown-item" href="listaclientes.php">Clientes</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="listafuncionarios.php">Funcionários</a>
          <a class="dropdown-item" href="comissao.php">Comissões</a>
        </div>
      </li>
        <li class="nav-item dropdown" style="margin-right: 30px;">
        <a class="nav-link dropdown-toggle" href="escolha.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style=" color: #FFFAFA; font-family: Century Gothic;">
          <i class="fas fa-user-circle"></i> <?=$_SESSION['usuarioNome']?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php">Sair</a>
          <a class="dropdown-item" href="agenda.php">Redefinir Senha</a>

        </div>
      </li>

      </ul>
    </div>
  </nav>
<br>
 <div class="p" id="p" style="text-align: center; font-size: 20px;">
     <p id="p"> <strong>Alterar agenda</strong> </p>
   </div>

   <hr>
  <div class="col pt-lg-1 pb-2" id="divexterna">
    <div class="col-lg-6 col-sm-12 mt-5 col-md-6 p-0 " id="divinterna" >
   <form method="post">
<input type="hidden" name="id" value="<?php echo $row_agenda['Id']; ?>">
  <div class="form-group">
   <div class="form-row mt-3 ">
          <div class="form-group col-md-4">
           <label for="inputCPF">Cliente</label>
            <input type="text" class="form-control" id="cliente" placeholder="Cliente" name="cliente" value="<?php echo $row_agenda['Cliente']; ?>" disabled>
          </div>
          <div class="form-group col-md-4">
           <label for="inputCPF">Horário</label>
            <input type="text" class="form-control" id="hr" placeholder="Ex.: 12:00" name="hr" value="<?php echo $row_agenda['Horario']; ?>">
          </div>

          <div class="form-group col-md-4">
           <label for="inputCPF">Data</label>
            <input type="text" class="form-control" id="data" placeholder="Data" name="data" value="<?php echo $row_agenda['Data']; ?>">
          </div>
</div>

 <div class="form-row mt-3 ">
     <div class="form-group col-md-4">
           <label for="inputCPF">Procedimento</label>
            <input type="text" class="form-control" id="procedimento" placeholder="Procedimento" name="procedimento" value="<?php echo $row_agenda['Procedimento']; ?>">
          </div>
         <div class="form-group col-md-4">
           <label for="inputCPF">Forma de Pagamento</label>
            <input type="text" class="form-control" id="fp" placeholder="Forma de Pagamento" name="fp" value="<?php echo $row_agenda['Formapg']; ?>">
          </div>
          <div class="form-group col-md-4">
           <label for="inputCPF">À receber</label>
            <input type="text" class="form-control" id="valor" placeholder="Ex.: R$: 150,00" name="valor" value="<?php echo $row_agenda['Receber']; ?>">
          </div>


</div>

 <div class="form-row mt-3 ">

          <div class="form-group col-md-4">
           <label for="inputCPF">Telefone Cliente</label>
            <input type="text" class="form-control" id="telefone" placeholder="Telefone" disabled name="telefone" value="<?php echo $row_agenda['Telefone']; ?>">
          </div>

          <div class="form-group col-md-4" style="display: none;">
           <label for="inputCPF">Status:</label>
            <input type="text" class="form-control" id="status" placeholder="Ativo" name="status" value="ATIVO" disabled>
          </div>
</div>
<br>
<div class="col-lg-12" align="center" >

          <button type="submit" class="btn btn-light" name="btn_alterar" id="btn_alterar" style="background-color: #DC8CF2; color: white;">Alterar</button>
          <a href="escolha.php"><button type="button" class="btn btn-light" style="background-color: #F28CCB; color: white;">Cancelar</button></a>
   </div>

</div>
</div>
</div>
</form>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>