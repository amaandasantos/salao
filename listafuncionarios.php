<?php

session_start();

if (isset($_SESSION['usuarioNome'])) {

}else{
  header('location: index.php');
}

require_once 'crud/crud_funcionarios.php';
require_once 'crud/crud_comissao.php';
require_once 'bancodedados/conexao.php';

$resultado_funcionario =  "SELECT * FROM funcionario WHERE Status = 'Ativo'";
$resultados =  mysqli_query($conn, $resultado_funcionario);


//ESTANCIANDO A CLASSES
$objFunc = new funcionarios();
$obj = new comissao();

if(isset($_POST['btn_modal'])){
  $objFunc->update($_POST);
    header('location: listafuncionarios.php');
 }

if(isset($_POST['btn_cadastro'])){
  $obj->Insert($_POST);
    header('location: listafuncionarios.php');
 }


?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <link rel="stylesheet" href="css/escolhas.css">
     <link rel="stylesheet" href="css/clientes.css">
     <link rel="stylesheet" type="text/css" href="css/universal.css">
     <link rel="stylesheet" type="text/css" href="css/agenda.css">
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
     <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
     <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
     <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Lista funcionários</title>
  </head>
  <body>
     <nav class="navbar navbar-expand-lg navbar-dark" style="background-color: #cd84f1; font-family: Century Gothic;">
    <a class="navbar-brand" href="escolha.php" style="color: #FFFAFA; font-size: 17px;">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse justify-content-center" id="navbarNav">

      <ul class="navbar-nav">
        <li class="nav-item" style="margin-right: 30px;">
          <a class="nav-link" href="escolha.php" style="color: #FFFAFA; font-size: 17px;">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item" style="margin-right: 30px;">
          <a class="nav-link" href="#" style="color: #FFFAFA; font-size: 17px;">Fidelidade</a>
        </li>
       <li class="nav-item dropdown" style="margin-right: 30px;">
        <a class="nav-link dropdown-toggle" href="escolha.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #FFFAFA; font-size: 17px;">
          Financeiro
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="despesa.php" style="font-size: 15px;">Despesas</a>
          <a class="dropdown-item" href="comissao.php" style="font-size: 15px;">Comissão</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="pagamentosrealizados.php" style="font-size: 15px;">Pagamentos Realizados</a>
          <a class="dropdown-item" href="despesa.php" style="font-size: 15px;">Pagamentos não realizados</a>
        </div>
      </li>
          <li class="nav-item dropdown" style="margin-right: 30px;">
        <a class="nav-link dropdown-toggle" href="escolha.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #FFFAFA; font-size: 17px;">
          Cadastro
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="clientes.php" style="font-size: 15px;">Cliente</a>
          <a class="dropdown-item" href="agenda.php" style="font-size: 15px;">Agenda</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="funcionarios.php" style="font-size: 15px;">Funcionário</a>
        </div>
      </li>
        <li class="nav-item dropdown" style="margin-right: 30px;">
        <a class="nav-link dropdown-toggle" href="escolha.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #FFFAFA; font-size: 17px;">
         Visualizar
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="listaagenda.php" style="font-size: 15px;">Agenda</a>
          <a class="dropdown-item" href="listaclientes.php" style="font-size: 15px;">Clientes</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="listafuncionarios.php" style="font-size: 15px;">Funcionários</a>
          <a class="dropdown-item" href="comissao.php" style="font-size: 15px;">Comissões</a>
        </div>
      </li>
        <li class="nav-item dropdown" style="margin-right: 30px;">
        <a class="nav-link dropdown-toggle" href="escolha.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #FFFAFA; font-size: 17px;">
          <i class="fas fa-user-circle"></i> <?=$_SESSION['usuarioNome']?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php" style="font-size: 15px;">Sair</a>
          <a class="dropdown-item" href="agenda.php" style="font-size: 15px;">Redefinir Senha</a>

        </div>
      </li>

      </ul>
    </div>
  </nav>

<br>
  <div class="p" id="p" style="text-align: center; font-size: 20px; font-family: 'Ubuntu Mono', monospace; color: #FF1493;">
     <p id="p"> <strong>Meus Funcionários</strong> </p>
   </div>

   <hr>
   <div class="container">
<a href="funcionarios.php"><button type="button" class="btn btn-light"  data-toggle="tooltip" title="Cadastrar novo funcionário" style="color: #FFF; background-color: #FE2E9A; border-radius: 5px;"><i class="fas fa-plus-circle"></i></button></a>
<button type="button" class="btn btn-light"   title="Comissão" data-toggle="modal" data-target="#exampleModal" style="color: #FFF; background-color: #FA5882; border-radius: 5px;"><i class="fas fa-hand-holding-usd"></i></button></a>
<a href="escolha.php"><button type="button" class="btn btn-light"  data-toggle="tooltip" title="Voltar para tela inicial" style="color: #FFF; background-color: #B40404; border-radius: 5px;"><i class="fas fa-sign-out-alt"></i></button></a>
<hr>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Comissão</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post">
          <div class="form-group">
   <div class="form-row mt-3 ">
          <div class="form-group col-md-12">
            <label for="inputNomeCompleto">Funcionário:</label>
           <select class="form-control" name="funcionario">
           <option value=""></option>
       <?php
       $resultado_funcionario = "SELECT * FROM funcionario";
       $result = mysqli_query($conn, $resultado_funcionario);
        while($row = mysqli_fetch_assoc($result)) { ?>
         <option value="<?php echo $row['Nome']; ?>"><?php echo $row['Nome']; ?></option> <?php

      }

       ?>
            </select>
          </div>
          <div class="form-group col-md-6">
           <label for="inputCPF">Comissão no valor de:</label>
            <input type="text" class="form-control" id="comissao" placeholder="Ex.: 120,00" name="comissao">
          </div>

          <div class="form-group col-md-6">
           <label for="inputCPF">Data do pagamento</label>
            <input type="date" class="form-control" id="data" placeholder="Data" name="data">
          </div>
</div>

 <div class="form-row mt-3 ">
     <div class="form-group col-md-12">
           <label for="inputCPF">Situação pagamento:</label>
            <select class="form-control" name="situacao">
              <option value=""></option>
              <option>Realizado</option>
              <option>Não realizado</option>
              <option>Outro</option>
            </select>
          </div>
          <div class="form-group col-md-6">
            <label for="inputNomeCompleto">Pago por:</label>
            <input type="text" class="form-control" id="pagante" placeholder="Pago por" name="pagante">
          </div>
          <div class="form-group col-md-6">
           <label for="inputCPF">Forma do pagamento</label>
            <input type="text" class="form-control" id="forma" placeholder="Ex.: Dinheiro" name="forma">
          </div>


</div>

<br>
<div class="col-lg-12" align="center" >
          <button type="submit" class="btn btn-light btn-lg" name="btn_cadastro" id="btn_cadastro;" style="background-color: #DF01A5; color: #FFF; border-radius: 4px;" >Cadastrar</button>
          <button type="button" class="btn btn-light btn-lg" data-dismiss="modal" style="background-color: #DF013A; color: #FFF; border-radius: 4px;">Cancelar</button>
   </div>

</div>
</form>
      </div>
      <div class="modal-footer">


      </div>
    </div>
  </div>
</div>

    <div class="agenda">
        <div class="table-responsive">
            <table class="table table-condensed table-bordered">
                <thead>
                    <tr>
                        <th style="font-size: 15px; text-align: center; font-family: Century Gothic;">Nome</th>
                        <th style="font-size: 15px; text-align: center; font-family: Century Gothic;">Rg</th>
                        <th style="font-size: 15px; text-align: center; font-family: Century Gothic;">Telefone</th>
                        <th style="font-size: 15px; text-align: center; font-family: Century Gothic;">E-mail</th>
                    </tr>
                </thead>
                <tbody >
                    <?php while ($row = mysqli_fetch_assoc($resultados)) { ?>
                    <!-- Single event in a single day -->
                    <tr>
                        <td class="agenda-date" class="active" rowspan="1" style="font-size: 15px; text-align: center; font-family: 'Ubuntu Mono', monospace;">
                            <?php echo $row['Nome'];?></div>

                        </td>
                        <td class="agenda-time" style="font-size: 15px; text-align: center; font-family: 'Ubuntu Mono', monospace;">
                           <?php echo $row['RG'];?>
                        </td>
                        <td class="agenda-events" style="font-size: 15px; text-align: center; font-family: 'Ubuntu Mono', monospace;">
                            <div class="agenda-event">
                                 <?php echo $row['Telefone'];?>
                            </div>
                        </td>
                         <td class="agenda-events" style="font-size: 15px; text-align: center; font-family: 'Ubuntu Mono', monospace;">
                            <div class="agenda-event">
                                 <?php echo $row['Email'];?>
                            </div>
                        </td>



                        <td class="actions" style="font-size: 28px;">
                          <a data-toggle="modal" data-target="#modal" title="Pagamento" class="btn btn-success" style="background-color: #3ADF00;" onclick="setaDadosModal('<?php echo $row["Id"]; ?>')">
                         <span class="btn-label" style="color: #FFF;"><i class="fa fa-check"></i></span>
                         </a>

<a href='editafuncionario.php?id=" . <?php echo $row['Id'];?> . "'><button type="button" class="btn btn-danger" data-toggle="tooltip" title="Editar dados do funcionário"><i class="fas fa-user-edit"></i></button></a>
                          <!--<button type="button" class="btn btn-warning" alt="Finalizado"><i class="fas fa-ban"></i></i></button>
                          <button type="button" class="btn btn-danger" alt="Finalizado"><i class="fas fa-trash-alt"></i></button> -->

          </td>
                    </tr>
                    <?php } ?>
                    <!-- Multiple events in a single day (note the rowspan) -->

                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
function setaDadosModal(valor) {
    document.getElementById('id').value = valor;
}
</script>

<div id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Relatar funcionário</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
            <div class="modal-body">
                <div class="panel-body">
                    <form id="modalExemplo" method="POST" action="">
                        <input type="text" name="id" id="id" style="display: none;">

                        <div class="form-group col-md-12">
           <label for="inputCPF">Salário</label>
            <select class="form-control" name="salario">
              <option value=""></option>
              <option>Pago</option>
              <option>Atrasado</option>
            </select>
          </div>

           <div class="form-group col-md-4">
           <label for="inputValor">Valor pago:</label>
            <input type="text" class="form-control" id="valor" placeholder="EX.: R$ 150,00" name="valor" >
          </div>
            <div class="form-group col-md-4">
           <label for="inputValor">Data Pagamento:</label>
            <input type="date" class="form-control" id="data" name="data" >
          </div>
          <div class="form-group col-md-4">
           <label for="inputValor">Pago por:</label>
            <input type="text" class="form-control" id="nome" name="nome" >
          </div>
          <div class="form-group col-md-12">
           <label for="inputValor">Observação:</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" name="observacao" rows="3"></textarea>
          </div>
<div class="col-lg-12" align="center" >

            <button type="submit" class="btn btn-info" id="btn_modal" name="btn_modal">Salvar</button>
         <button type="submit" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
   </div>
        </form>


      </div>

                </div>
            </div>
        </div>
    </div>
</div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>