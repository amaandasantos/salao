<?php

    include_once("bancodedados/conexao.php");
    $html = '<table width="500" border="1" cellspacing="0" cellpadding="1" style="margin-left:40px;"';
    $html .= '<thead>';
    $html .= '<tr>';
    $html .= '<th>Descrição</th>';
    $html .= '<th>Data</th>';
    $html .= '<th>Valor</th>';
    $html .= '<th>Forma do Pagamento</th>';
    $html .= '<th>Observação</th>';
    $html .= '<th>Status</th>';
    $html .= '</tr>';
    $html .= '</thead>';
    $html .= '<tbody>';


   $resultado_despesa =  "SELECT * FROM despesa WHERE Status = 'Ativo'";
   $resultados =  mysqli_query($conn, $resultado_despesa);
    while($row_d = mysqli_fetch_assoc($resultados)){
        $html .= '<tr><td style="text-align: center;">'.$row_d['Descricao'] . "</td>";
        $html .= '<td style="text-align: center;">'. date("d/m/Y", strtotime($row_d['Data'])). "</td>";
        $html .= '<td style="text-align: center;">'.$row_d['Valor'] . "</td>";
        $html .= '<td style="text-align: center;">'.$row_d['Pagamento'] . "</td>";
        $html .= '<td style="text-align: center;">'.$row_d['Observacao'] . "</td>";
        $html .= '<td style="text-align: center;">'.$row_d['Status'] . "</td></tr>";
    }

    $html .= '</tbody>';
    $html .= '</table';


    //referenciar o DomPDF com namespace
    use Dompdf\Dompdf;

    // include autoloader
    require_once("dompdf/autoload.inc.php");

    //Criando a Instancia
    $dompdf = new DOMPDF();

    // Carrega seu HTML
$dompdf->load_html('
            <img src="img/salao.jpg" alt="" style="width:150px; height:50px; margin-left:280px;">
            <h1 style="text-align: center;">Relátorio - Despesas em aberto &nbsp;&nbsp; </h1>
            '. $html .'
        ');


    //Renderizar o html
    $dompdf->render();

    //Exibibir a página
    $dompdf->stream(
        "relatorio_celke.pdf",
        array(
            "Attachment" => false //Para realizar o download somente alterar para true
        )
    );

?>