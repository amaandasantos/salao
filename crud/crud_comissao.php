<?php

require_once('bancodedados/Conexao.class.php');
require_once('bancodedados/Funcoes.class.php');

class comissao {
    private $id;
    private $funcionario;
    private $comissao;
    private $data;
    private $situacao;
    private $pagante;
    private $forma;


    public function __construct(){
        $this->con = new Conexao();
        $this->objfc = new Funcoes();
    }

    //METODOS MAGICO
    public function __set($atributo, $valor){
        $this->$atributo = $valor;
    }
    public function __get($atributo){
        return $this->$atributo;
    }


    public function Insert($dados){


          $this->funcionario = $dados['funcionario'];
          $this->comissao = $dados['comissao'];
          $this->data = $dados['data'];
          $this->situacao = $dados['situacao'];
          $this->pagante = $dados['pagante'];
          $this->forma = $dados['forma'];

          try{
          $cst = $this->con->conectar()->prepare("INSERT INTO `comissao` (`Funcionario`, `Comissao`, `Data`, `Situacao`, `Pagante`, `Forma`) VALUES (:funcionario, :comissao, :data, :situacao, :pagante, :forma);");


            $cst->bindParam(":funcionario", $this->funcionario, PDO::PARAM_STR);
            $cst->bindParam(":comissao", $this->comissao, PDO::PARAM_STR);
            $cst->bindParam(":data", $this->data, PDO::PARAM_STR);
            $cst->bindParam(":situacao", $this->situacao, PDO::PARAM_STR);
            $cst->bindParam(":pagante", $this->pagante, PDO::PARAM_STR);
            $cst->bindParam(":forma", $this->forma, PDO::PARAM_STR);

            $cst->execute();
            }catch(PDOException $e){
            return 'Error: '.$e->getMessage();
        }
    }
}
?>