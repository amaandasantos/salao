<?php

require_once('bancodedados/Conexao.class.php');
require_once('bancodedados/Funcoes.class.php');

class Login
{

   private $email;
   private $senha;
   private $con;
   private $nome;

    //CONSTRUTOR
    public function __construct(){
        $this->con = new Conexao();
        $this->objfc = new Funcoes();
    }
    //METODOS MAGICO
    public function __set($atributo, $valor){
        $this->$atributo = $valor;
    }
    public function __get($atributo){
        return $this->$atributo;
    }

    public function logaUsuario($dados){
        $this->email = $dados['email'];
        $this->senha = $dados['senha'];

        try{
            $cst = $this->con->conectar()->prepare("SELECT * FROM usuario WHERE `Email` = :email AND `Senha` = :senha;");
            $cst->bindParam(':email', $this->email, PDO::PARAM_STR);
            $cst->bindParam(':senha', $this->senha, PDO::PARAM_STR);
            $cst->execute();
            $rst = $cst->fetch();
            if($cst->rowCount() == 0){
              echo  "<script>alert('Email enviado com Sucesso!);</script>";
            }else{
                session_start();
                $_SESSION['logado'] = "sim";
                $_SESSION['func'] = $rst['Id'];
                $_SESSION['usuarioNome'] = $rst['Nome'];
                $_SESSION['usuarioEmail'] = $rst['Email'];
                $_SESSION['senha'] = $rst['Senha'];
                header("location: escolha.php");
        }


        }catch(PDOException $e){
            return 'Error: '.$e->getMassage();
        }

    }


    public function sairUsuario(){
        session_destroy();
        header ('location: index.php');
    }

}


 ?>