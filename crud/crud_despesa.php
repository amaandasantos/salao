<?php

require_once('bancodedados/Conexao.class.php');
require_once('bancodedados/Funcoes.class.php');

class despesa {
    private $id;
    private $descricao;
    private $data;
    private $pagamento;
    private $valor;
    private $observacao;


    public function __construct(){
        $this->con = new Conexao();
        $this->objfc = new Funcoes();
    }

    //METODOS MAGICO
    public function __set($atributo, $valor){
        $this->$atributo = $valor;
    }
    public function __get($atributo){
        return $this->$atributo;
    }


    public function Insert($dados){


          $this->descricao = $dados['descricao'];
          $this->data = $dados['data'];
          $this->pagamento = $dados['pagamento'];
          $this->valor = $dados['valor'];
          $this->observacao = $dados['observacao'];
          $this->status = Ativo;


          try{
          $cst = $this->con->conectar()->prepare("INSERT INTO `despesa` (`Descricao`, `Data`, `Pagamento`, `Valor`, `Observacao`,`Status`) VALUES (:descricao, :data, :pagamento, :valor, :observacao, :status);");


            $cst->bindParam(":descricao", $this->descricao, PDO::PARAM_STR);
            $cst->bindParam(":data", $this->data, PDO::PARAM_STR);
            $cst->bindParam(":pagamento", $this->pagamento, PDO::PARAM_STR);
            $cst->bindParam(":valor", $this->valor, PDO::PARAM_STR);
            $cst->bindParam(":observacao", $this->observacao, PDO::PARAM_STR);
            $cst->bindParam(":status", $this->status, PDO::PARAM_STR);

            $cst->execute();
            }catch(PDOException $e){
            return 'Error: '.$e->getMessage();
        }
    }

    public function Update($dados){

          $this->id = $dados['id'];
          $this->descricao = $dados['descricao'];
          $this->data = $dados['data'];
          $this->pagamento = $dados['pagamento'];
          $this->valor = $dados['valor'];
          $this->observacao = $dados['observacao'];
          $this->status = Ativo;


          try{
          $cst = $this->con->conectar()->prepare("UPDATE `despesa` SET  `Descricao` = :descricao, `Data` = :data, `Pagamento` = :pagamento, `Valor` = :valor, `Observacao` = :observacao, `Status` = :status WHERE `Id` = :id;");


            $cst->bindParam(":id", $this->id, PDO::PARAM_INT);
            $cst->bindParam(":descricao", $this->descricao, PDO::PARAM_STR);
            $cst->bindParam(":data", $this->data, PDO::PARAM_STR);
            $cst->bindParam(":pagamento", $this->pagamento, PDO::PARAM_STR);
            $cst->bindParam(":valor", $this->valor, PDO::PARAM_STR);
            $cst->bindParam(":observacao", $this->observacao, PDO::PARAM_STR);
            $cst->bindParam(":status", $this->status, PDO::PARAM_STR);

            $cst->execute();
            }catch(PDOException $e){
            return 'Error: '.$e->getMessage();
        }
    }

    public function UpdatePagamento($dados){

          $this->id = $dados['id'];
          $this->statusP = $dados['statusP'];
          $this->dataP = $dados['dataP'];
          $this->obspagamento = $dados['obspagamento'];


          try{
          $cst = $this->con->conectar()->prepare("UPDATE `despesa` SET  `Status` = :statusP, `DataPago` = :dataP, `ObsPagamento` = :obspagamento WHERE `Id` = :id;");


            $cst->bindParam(":id", $this->id, PDO::PARAM_INT);
            $cst->bindParam(":statusP", $this->statusP, PDO::PARAM_STR);
            $cst->bindParam(":dataP", $this->dataP, PDO::PARAM_STR);
            $cst->bindParam(":obspagamento", $this->obspagamento, PDO::PARAM_STR);


            $cst->execute();
            }catch(PDOException $e){
            return 'Error: '.$e->getMessage();
        }
    }
}
?>