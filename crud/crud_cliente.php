<?php

require_once('bancodedados/Conexao.class.php');
require_once('bancodedados/Funcoes.class.php');

class cliente {
    private $id;
    private $nome;
    private $cpf;
    private $rg;
    private $rua;
    private $bairro;
    private $cep;
    private $numero;
    private $telefone;
    private $status;

    public function __construct(){
        $this->con = new Conexao();
        $this->objfc = new Funcoes();
    }

    //METODOS MAGICO
    public function __set($atributo, $valor){
        $this->$atributo = $valor;
    }
    public function __get($atributo){
        return $this->$atributo;
    }


    public function Insert($dados){


          $this->nome = $dados['nome'];
          $this->cpf = $dados['cpf'];
          $this->rg = $dados['rg'];
          $this->rua = $dados['rua'];
          $this->bairro = $dados['bairro'];
          $this->cep = $dados['cep'];
          $this->numero = $dados['numero'];
          $this->telefone = $dados['telefone'];
          $this->status = Ativo;


          try{
          $cst = $this->con->conectar()->prepare("INSERT INTO `cliente` (`Nome`, `CPF`, `RG`, `Rua`, `Bairro`, `CEP`, `Numero`,`Telefone`,`Status`) VALUES (:nome, :cpf, :rg, :rua, :bairro, :cep, :numero, :telefone, :status);");


            $cst->bindParam(":nome", $this->nome, PDO::PARAM_STR);
            $cst->bindParam(":cpf", $this->cpf, PDO::PARAM_STR);
            $cst->bindParam(":rg", $this->rg, PDO::PARAM_STR);
            $cst->bindParam(":rua", $this->rua, PDO::PARAM_STR);
            $cst->bindParam(":bairro", $this->bairro, PDO::PARAM_STR);
            $cst->bindParam(":cep", $this->cep, PDO::PARAM_STR);
            $cst->bindParam(":numero", $this->numero, PDO::PARAM_STR);
            $cst->bindParam(":telefone", $this->telefone, PDO::PARAM_STR);
            $cst->bindParam(":status", $this->status, PDO::PARAM_STR);

            $cst->execute();
        }catch(PDOException $e){
            return 'Error: '.$e->getMessage();
        }
    }

     public function updateFuncionario($dados){

          $this->id = $dados['id'];
          $this->nome = $dados['nome'];
          $this->cpf = $dados['cpf'];
          $this->rg = $dados['rg'];
          $this->rua = $dados['rua'];
          $this->bairro = $dados['bairro'];
          $this->cep = $dados['cep'];
          $this->numero = $dados['numero'];
          $this->telefone = $dados['telefone'];
          $this->status = Ativo;


          try{
          $cst = $this->con->conectar()->prepare("UPDATE `cliente` SET  `Nome` = :nome, `CPF` = :cpf, `RG` = :rg, `Rua` = :rua, `Bairro` = :bairro, `CEP` = :cep, `Numero` = :numero, `Telefone` = :telefone, `Status` = :status WHERE `Id` = :id;");

            $cst->bindParam(":id", $this->id, PDO::PARAM_INT);
            $cst->bindParam(":nome", $this->nome, PDO::PARAM_STR);
            $cst->bindParam(":cpf", $this->cpf, PDO::PARAM_STR);
            $cst->bindParam(":rg", $this->rg, PDO::PARAM_STR);
            $cst->bindParam(":rua", $this->rua, PDO::PARAM_STR);
            $cst->bindParam(":bairro", $this->bairro, PDO::PARAM_STR);
            $cst->bindParam(":cep", $this->cep, PDO::PARAM_STR);
            $cst->bindParam(":numero", $this->numero, PDO::PARAM_STR);
            $cst->bindParam(":telefone", $this->telefone, PDO::PARAM_STR);
            $cst->bindParam(":status", $this->status, PDO::PARAM_STR);

            $cst->execute();
        }catch(PDOException $e){
            return 'Error: '.$e->getMessage();
        }
    }


}
?>