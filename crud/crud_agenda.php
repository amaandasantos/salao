

<?php

require_once('bancodedados/Conexao.class.php');
require_once('bancodedados/Funcoes.class.php');

class agenda {
    private $id;
    private $cliente;
    private $hr;
    private $data;
    private $procedimento;
    private $fp;
    private $valor;
    private $telefone;
    private $status;


    public function __construct(){
        $this->con = new Conexao();
        $this->objfc = new Funcoes();
    }

    //METODOS MAGICO
    public function __set($atributo, $valor){
        $this->$atributo = $valor;
    }
    public function __get($atributo){
        return $this->$atributo;
    }


    public function Insert($dados){


          $this->cliente = $dados['cliente'];
          $this->hr = $dados['hr'];
          $this->data = $dados['data'];
          $this->procedimento = $dados['procedimento'];
          $this->fp = $dados['fp'];
          $this->valor = $dados['valor'];
          $this->telefone = $dados['telefone'];
          $this->status = Ativo;


          try{
          $cst = $this->con->conectar()->prepare("INSERT INTO `agenda` (`Cliente`, `Horario`, `Data`, `Procedimento`, `Formapg`, `Receber`,`Telefone`,`Status`) VALUES (:cliente, :hr, :data, :procedimento, :fp, :valor,  :telefone, :status);");


            $cst->bindParam(":cliente", $this->cliente, PDO::PARAM_STR);
            $cst->bindParam(":hr", $this->hr, PDO::PARAM_STR);
            $cst->bindParam(":data", $this->data, PDO::PARAM_STR);
            $cst->bindParam(":procedimento", $this->procedimento, PDO::PARAM_STR);
            $cst->bindParam(":fp", $this->fp, PDO::PARAM_STR);
            $cst->bindParam(":valor", $this->valor, PDO::PARAM_STR);
            $cst->bindParam(":telefone", $this->telefone, PDO::PARAM_STR);
            $cst->bindParam(":status", $this->status, PDO::PARAM_STR);

            $cst->execute();
        }catch(PDOException $e){
            return 'Error: '.$e->getMessage();
        }
    }

      public function updateAgenda($dados){

          $this->id = $dados['id'];
          /*$this->cliente = $dados['cliente'];*/
          $this->hr = $dados['hr'];
          $this->data = $dados['data'];
          $this->procedimento = $dados['procedimento'];
          $this->valor = $dados['valor'];
          $this->fp = $dados['fp'];
          /*

          $this->telefone = $dados['telefone'];
          $this->status = Ativo;*/


          try{
          $cst = $this->con->conectar()->prepare("UPDATE `agenda` SET   `Horario` = :hr, `Procedimento` = :procedimento, `Data` = :data, `Receber` = :valor, `Formapg` = :fp  WHERE `Id` = :id;");

            $cst->bindParam(":id", $this->id, PDO::PARAM_INT);
            //$cst->bindParam(":cliente", $this->cliente, PDO::PARAM_STR);
            $cst->bindParam(":hr", $this->hr, PDO::PARAM_STR);
            $cst->bindParam(":data", $this->data, PDO::PARAM_STR);
            $cst->bindParam(":procedimento", $this->procedimento, PDO::PARAM_STR);
            $cst->bindParam(":valor", $this->valor, PDO::PARAM_STR);
            $cst->bindParam(":fp", $this->fp, PDO::PARAM_STR);
            /*

            $cst->bindParam(":telefone", $this->telefone, PDO::PARAM_STR);
            $cst->bindParam(":status", $this->status, PDO::PARAM_STR);*/

            $cst->execute();
        }catch(PDOException $e){
            return 'Error: '.$e->getMessage();
        }
    }

    public function update($dados){

        $this->id = $dados['id'];
        $this->status = $dados['status'];
        $this->valor = $dados['valor'];
        $this->data = $dados['data'];
        $this->pagamento = $dados['pagamento'];
        $this->observacao = $dados['observacao'];

        try {
            $cst = $this->con->conectar()->prepare("UPDATE `agenda` SET  `Status` = :status, `Receber` = :valor, `DataRecebimento` = :data, `Observacao` = :observacao, `Pagamento` = :pagamento WHERE `Id` = :id;");


            $cst->bindParam(":id", $this->id, PDO::PARAM_INT);
            $cst->bindParam(":status", $this->status, PDO::PARAM_STR);
            $cst->bindParam(":valor", $this->valor, PDO::PARAM_STR);
            $cst->bindParam(":data", $this->data, PDO::PARAM_STR);
            $cst->bindParam(":pagamento", $this->pagamento, PDO::PARAM_STR);
            $cst->bindParam(":observacao", $this->observacao, PDO::PARAM_STR);


            $cst->execute();
        }catch(PDOException $e){
            return 'Error: '.$e->getMessage();
        }
    }
 }


?>

