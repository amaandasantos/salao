

<?php

require_once('bancodedados/Conexao.class.php');
require_once('bancodedados/Funcoes.class.php');

class funcionarios {
    private $id;
    private $nome;
    private $CPF;
    private $RG;
    private $dt;
    private $idade;
    private $sexo;
    private $telefone;
    private $email;
    private $rua;
    private $bairro;
    private $CEP;
    private $cidade;
    private $uf;
    private $nacionalidade;
    private $numero;
    private $complemento;


    public function __construct(){
        $this->con = new Conexao();
        $this->objfc = new Funcoes();
    }

    //METODOS MAGICO
    public function __set($atributo, $valor){
        $this->$atributo = $valor;
    }
    public function __get($atributo){
        return $this->$atributo;
    }


    public function Insert($dados){


          $this->nome = $dados['nome'];
          $this->cpf = $dados['cpf'];
          $this->rg = $dados['rg'];
          $this->dt = $dados['dt'];
          $this->idade = $dados['idade'];
          $this->sexo = $dados['sexo'];
          $this->telefone = $dados['telefone'];
          $this->email = $dados['email'];
          $this->rua = $dados['rua'];
          $this->bairro = $dados['bairro'];
          $this->cep = $dados['cep'];
          $this->cidade = $dados['cidade'];
          $this->uf = $dados['uf'];
          $this->nacionalidade = $dados['nacionalidade'];
          $this->numero = $dados['numero'];
          $this->complemento = $dados['complemento'];
          $this->status = Ativo;


          try{
          $cst = $this->con->conectar()->prepare("INSERT INTO `funcionario` (`Nome`, `CPF`, `RG`, `DataNascimento`, `Idade`, `Sexo`,`Telefone`, `Email`, `Rua`, `Bairro`, `CEP`, `Cidade`, `UF`, `Nacionalidade`, `Numero`, `Complemento`, `Status`) VALUES (:nome, :cpf, :rg, :dt, :idade, :sexo,  :telefone, :email, :rua, :bairro, :cep, :cidade, :uf, :nacionalidade, :numero, :complemento, :status);");


            $cst->bindParam(":nome", $this->nome, PDO::PARAM_STR);
            $cst->bindParam(":cpf", $this->cpf, PDO::PARAM_STR);
            $cst->bindParam(":rg", $this->rg, PDO::PARAM_STR);
            $cst->bindParam(":dt", $this->dt, PDO::PARAM_STR);
            $cst->bindParam(":idade", $this->idade, PDO::PARAM_STR);
            $cst->bindParam(":sexo", $this->sexo, PDO::PARAM_STR);
            $cst->bindParam(":telefone", $this->telefone, PDO::PARAM_STR);
            $cst->bindParam(":email", $this->email, PDO::PARAM_STR);
            $cst->bindParam(":rua", $this->rua, PDO::PARAM_STR);
            $cst->bindParam(":bairro", $this->bairro, PDO::PARAM_STR);
            $cst->bindParam(":cep", $this->cep, PDO::PARAM_STR);
            $cst->bindParam(":cidade", $this->cidade, PDO::PARAM_STR);
            $cst->bindParam(":uf", $this->uf, PDO::PARAM_STR);
            $cst->bindParam(":nacionalidade", $this->nacionalidade, PDO::PARAM_STR);
            $cst->bindParam(":numero", $this->numero, PDO::PARAM_STR);
            $cst->bindParam(":complemento", $this->complemento, PDO::PARAM_STR);
            $cst->bindParam(":status", $this->status, PDO::PARAM_STR);

            $cst->execute();
        }catch(PDOException $e){
            return 'Error: '.$e->getMessage();
        }
    }

    public function updateFuncionario($dados){

          $this->id = $dados['id'];
          $this->nome = $dados['nome'];
          $this->cpf = $dados['cpf'];
          $this->rg = $dados['rg'];
          $this->dt = $dados['dt'];
          $this->idade = $dados['idade'];
          $this->sexo = $dados['sexo'];
          $this->telefone = $dados['telefone'];
          $this->email = $dados['email'];
          $this->rua = $dados['rua'];
          $this->bairro = $dados['bairro'];
          $this->cep = $dados['cep'];
          $this->cidade = $dados['cidade'];
          $this->uf = $dados['uf'];
          $this->nacionalidade = $dados['nacionalidade'];
          $this->numero = $dados['numero'];
          $this->complemento = $dados['complemento'];
          $this->status = Ativo;


          try{
          $cst = $this->con->conectar()->prepare("UPDATE `funcionario` SET  `Nome` = :nome, `CPF` = :cpf, `RG` = :rg, `DataNascimento` = :dt, `Idade` = :idade, `Sexo` = :sexo, `Telefone` = :telefone, `Email` = :email, `Rua` = :rua, `Bairro` = :bairro, `CEP` = :cep, `Cidade` = :cidade, `UF` = :uf, `Nacionalidade` = :nacionalidade, `Numero` = :numero, `Complemento` = :complemento, `Status` = :status WHERE `Id` = :id;");

            $cst->bindParam(":id", $this->id, PDO::PARAM_INT);
            $cst->bindParam(":nome", $this->nome, PDO::PARAM_STR);
            $cst->bindParam(":cpf", $this->cpf, PDO::PARAM_STR);
            $cst->bindParam(":rg", $this->rg, PDO::PARAM_STR);
            $cst->bindParam(":dt", $this->dt, PDO::PARAM_STR);
            $cst->bindParam(":idade", $this->idade, PDO::PARAM_STR);
            $cst->bindParam(":sexo", $this->sexo, PDO::PARAM_STR);
            $cst->bindParam(":telefone", $this->telefone, PDO::PARAM_STR);
            $cst->bindParam(":email", $this->email, PDO::PARAM_STR);
            $cst->bindParam(":rua", $this->rua, PDO::PARAM_STR);
            $cst->bindParam(":bairro", $this->bairro, PDO::PARAM_STR);
            $cst->bindParam(":cep", $this->cep, PDO::PARAM_STR);
            $cst->bindParam(":cidade", $this->cidade, PDO::PARAM_STR);
            $cst->bindParam(":uf", $this->uf, PDO::PARAM_STR);
            $cst->bindParam(":nacionalidade", $this->nacionalidade, PDO::PARAM_STR);
            $cst->bindParam(":numero", $this->numero, PDO::PARAM_STR);
            $cst->bindParam(":complemento", $this->complemento, PDO::PARAM_STR);
            $cst->bindParam(":status", $this->status, PDO::PARAM_STR);

            $cst->execute();
        }catch(PDOException $e){
            return 'Error: '.$e->getMessage();
        }
    }




    public function update($dados){

        $this->id = $dados['id'];
        $this->salario = $dados['salario'];
        $this->valor = $dados['valor'];
        $this->data = $dados['data'];
        $this->nome = $dados['nome'];
        $this->observacao = $dados['observacao'];

        try {
            $cst = $this->con->conectar()->prepare("UPDATE `funcionario` SET  `Salario` = :salario, `Valor` = :valor, `DataPagamento` = :data, `Observacao` = :observacao, `PagoPor` = :nome WHERE `Id` = :id;");


            $cst->bindParam(":id", $this->id, PDO::PARAM_INT);
            $cst->bindParam(":salario", $this->salario, PDO::PARAM_STR);
            $cst->bindParam(":valor", $this->valor, PDO::PARAM_STR);
            $cst->bindParam(":data", $this->data, PDO::PARAM_STR);
            $cst->bindParam(":nome", $this->nome, PDO::PARAM_STR);
            $cst->bindParam(":observacao", $this->observacao, PDO::PARAM_STR);


            $cst->execute();
        }catch(PDOException $e){
            return 'Error: '.$e->getMessage();
        }
    }
 }


?>

